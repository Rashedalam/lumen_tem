<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <!-- jQuery library -->

    <title>Hello, world!</title>
</head>
<body>



<div class="container">
    <table id="list_table" class="table table-bordered table-striped text-center">
        <tr>
            <th>Name</th>
            <th>Roll</th>
            <th>Department</th>
        </tr>


    </table>
</div>







<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<script>
    function index() {
        $.ajax({
            url : 'http://pondit_lumen.com/students',
            type : 'get',
            success : function (response) {
                let result = eval(response);
                for (let item of result){
                    let output = '<tr>\n' +
                        '    <td>'+item.name+'</td>\n' +
                        '    <td>'+item.roll+'</td>\n' +
                        '    <td>'+item.department+'</td>\n' +
                        '</tr>';
                    $("#list_table").append(output);
                }
            }
        })
    }
    index();
</script>
</body>
</html>
<!---->
